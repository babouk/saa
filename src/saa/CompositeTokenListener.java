/**
 * 
 */
package saa;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a composite token listener dispatching each
 * {@link #onToken(ITokenListener.Range, TokenNode)} method call to all
 * registered listeners.
 * 
 * @author kotelnikov
 * @param <S> the type of the value associated with tokens
 */
public class CompositeTokenListener<S> implements ITokenListener<S> {

    /**
     * The list of listeners. All calls to the
     * {@link #onToken(ITokenListener.Range, TokenNode)} method in this
     * object will be dispatched to all listeners in this list.
     */
    private List<ITokenListener<S>> fListeners = new ArrayList<ITokenListener<S>>();

    /**
     * 
     */
    public CompositeTokenListener() {
    }

    /**
     * Adds a new listener to the internal list
     * 
     * @param listener the listener to add
     */
    public synchronized void addListener(ITokenListener<S> listener) {
        List<ITokenListener<S>> list = new ArrayList<ITokenListener<S>>(
            fListeners);
        list.add(listener);
        fListeners = list;
    }

    /**
     * @see ITokenListener#onToken(ITokenListener.Range, TokenNode)
     */
    public void onToken(Range range, TokenNode<S> node) {
        List<ITokenListener<S>> list = fListeners;
        for (ITokenListener<S> listener : list) {
            listener.onToken(range, node);
        }
    }

    /**
     * Removes the given listener from the internal list
     * 
     * @param listener the listener to remove
     */
    public synchronized void removeListener(ITokenListener<S> listener) {
        List<ITokenListener<S>> list = new ArrayList<ITokenListener<S>>(
            fListeners);
        list.remove(listener);
        fListeners = list;
    }

}
