/**
 * 
 */
package saa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import saa.ITokenListener.Range;

/**
 * This class is used to build scores for tokens in the text.
 * 
 * @author kotelnikov
 * @param <S>
 */
public class TokenScorrer<S> implements ITokenDetector<S> {

    /**
     * Score object is used to accumulate references to all places in the text
     * where this token was found.
     * 
     * @author kotelnikov
     * @param <S> the type of the value associated with the token
     */
    public static class Score<S> implements Comparable<Score<S>> {

        /**
         * The list of references in the text where the token is found.
         */
        private List<Range> fRanges = new ArrayList<Range>();

        /**
         * The token
         */
        private TokenNode<S> fToken;

        /**
         * This constructor initializes the internal fields of this class.
         * 
         * @param token the token
         */
        public Score(TokenNode<S> token) {
            fToken = token;
        }

        /**
         * Adds a new range (pointer) defining a new position of the token in
         * the text
         * 
         * @param range the text range corresponding to this token
         */
        public void addRange(Range range) {
            fRanges.add(range);
        }

        /**
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(Score<S> o) {
            int delta = (o.fRanges.size() - fRanges.size());
            if (delta == 0) {
                delta = -fToken.compareTo(o.fToken);
            }
            return delta;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof TokenScorrer.Score<?>)) {
                return false;
            }
            TokenScorrer.Score<S> o = (TokenScorrer.Score<S>) obj;
            return compareTo(o) == 0;
        }

        /**
         * Returns the list of all ranges where the underlying token could be
         * found in the text.
         * 
         * @return the list of all ranges where the underlying token could be
         *         found in the text
         */
        public List<Range> getRanges() {
            return fRanges;
        }

        /**
         * Returns the token corresponding to this score.
         * 
         * @return the token corresponding to this score
         */
        public TokenNode<S> getToken() {
            return fToken;
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            int a = fToken != null ? fToken.hashCode() : 0;
            int b = fRanges.hashCode();
            return a ^ b;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return fToken + " : " + fRanges;
        }
    }

    /**
     * The internal listener used to update token scores.
     */
    private ITokenListener<S> fListener = new ITokenListener<S>() {
        public void onToken(Range range, TokenNode<S> node) {
            Score<S> score = fScores.get(node);
            if (score == null) {
                score = new Score<S>(node);
                fScores.put(node, score);
            }
            score.addRange(range);
        }
    };

    /**
     * This map contains tokens and the corresponding token scores (instances of
     * the {@link Score} type).
     */
    private Map<TokenNode<S>, Score<S>> fScores = new LinkedHashMap<TokenNode<S>, Score<S>>();

    /**
     * The internal token detector used by this class.
     */
    private ITokenDetector<S> fTokenDetector;

    /**
     * Initializes internal fields and sets the token detector (a
     * {@link ITokenDetector} instance) where all calls will be delegated.
     * 
     * @param tokenDetector
     */
    public TokenScorrer(ITokenDetector<S> tokenDetector) {
        setTokenDetector(tokenDetector);
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#addListener(org.mandriva.doc4.entities.ITokenListener)
     */
    public void addListener(ITokenListener<S> listener) {
        fTokenDetector.addListener(listener);
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#finish()
     */
    public void finish() {
        fTokenDetector.finish();
        setTokenDetector(null);
    }

    /**
     * Returns a sorted array of token scores found in the text.
     * 
     * @return a sorted array of token scores found in the text
     */
    public List<Score<S>> getScores() {
        ArrayList<Score<S>> list = new ArrayList<Score<S>>(fScores.values());
        Collections.sort(list);
        return list;
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#removeListener(org.mandriva.doc4.entities.ITokenListener)
     */
    public void removeListener(ITokenListener<S> listener) {
        fTokenDetector.removeListener(listener);
    }

    /**
     * Sets a new token detector where all calls will be delegated.
     * 
     * @param tokenDetector the token detector to set
     */
    public void setTokenDetector(ITokenDetector<S> tokenDetector) {
        if (fTokenDetector != null) {
            fTokenDetector.removeListener(fListener);
        }
        fTokenDetector = tokenDetector;
        if (fTokenDetector != null) {
            fTokenDetector.addListener(fListener);
        }
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#update(char)
     */
    public void update(char ch) {
        fTokenDetector.update(ch);
    }

}
