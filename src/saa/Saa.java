package saa;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import saa.TokenScorrer.Score;

public class Saa {

  protected static  void fillTokens(Map<String, Object> map, TokenTree<Object> tree, String... tokens) {
    for (String token : tokens) {
      String trimmed = token.trim();
      if (trimmed.length() > 0) {
        Object value = new Object();
        tree.add(trimmed, value);
        map.put(trimmed, value);
      }
    }
  }

  public static void main(String[] args) {

    String text = args[0];
    String tags = args[1];
    Map<String, Object> tokens = new LinkedHashMap<String, Object>();
    TokenTree<Object> tree = new TokenTree<Object>();
    fillTokens(tokens, tree, tags.split(","));

    ITokenDetector<Object> detector = new TokenDetector<Object>(tree);
    TokenScorrer<Object> scorrer = new TokenScorrer<Object>(detector);
    for (int i = 0; i < text.length(); i++) {
      char ch = text.charAt(i);
      scorrer.update(ch);
    }
    
    scorrer.finish();

    List<Score<Object>> scores = scorrer.getScores();

    System.out.println();
    System.out.println("Found tokens:");
    
    for (Score<Object> score : scores) {
      System.out.println(score.getToken() + " - " + score.getRanges());
    }

  }
}