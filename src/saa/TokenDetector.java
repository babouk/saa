/**
 * 
 */
package saa;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import saa.ITokenListener.Range;

/**
 * Objects of this type are used to detect tokens (named entities) in texts.
 * Token detectors consume characters provided by clients and notify registered
 * listeners when a new token is found. Example of usage:
 * 
 * <pre>
 * TokenTree<MyClass> tree = new TokenTree<MyClass>();
 * tree.add("this", new MyClass(123));
 * tree.add("text", new MyClass(345));
 * TokenDetector<MyClass> detector = 
 *      new TokenDetector<MyClass>(tree, new ITokenListener<MyClass>() {
 *          public void onToken(Range range, String token, MyClass value) {
 *              System.out.println(range + "\t" + token + "\t" + value);
 *          }
 *      });
 *  
 * String text = "this is my text";
 * for (int i=0; i<text.length(); i++) {
 *     char ch = text.charAt(i);
 *     // Push the character to the token detector.
 *     detector.update(ch);
 * }
 * // Finalize the process of token detection.
 * detector.finish();
 *   
 * </pre>
 * 
 * @author kotelnikov
 * @param <S> the type of objects associated with tokens
 */
public class TokenDetector<S> implements ITokenDetector<S> {

    /**
     * Objects of this type are created internally every time when a new
     * potential match with a token is found. This structure points to a node in
     * the {@link TokenTree} and contains also a reference to the start position
     * of the token. Instances of this type are updated every time when the
     * {@link #TokenDetector #update(char)} method is called. If this pointer
     * refers a node with a non-empty value object (see the
     * {@link TokenNode#getValue()} method) then it is considered that a new
     * token is found. This token is reported via the
     * {@link ITokenListener#onToken(Range, TokenNode)} method.
     * 
     * @author kotelnikov
     * @param <S> the value associated with found tokens
     */
    private static class TokenTreePointer<S> {

        /**
         * The start position of the token in the text.
         */
        private int fStartPos;

        /**
         * The current node in the token tree.
         */
        private TokenNode<S> fTokenNode;

        /**
         * The default constructor used to initialize the internal fields
         * 
         * @param node the initial node
         * @param startPos the start position of the token
         */
        public TokenTreePointer(TokenNode<S> node, int startPos) {
            fTokenNode = node;
            fStartPos = startPos;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        /**
         * Returns the start position of the token
         * 
         * @return the start position of the token
         */
        public int getStartPos() {
            return fStartPos;
        }

        /**
         * Return the token
         * 
         * @return the token
         */
        public String getToken() {
            return fTokenNode != null ? fTokenNode.toString() : null;
        }

        /**
         * Returns the token node associated with this pointer.
         * 
         * @return the token node associated with this pointer
         */
        public TokenNode<S> getTokenNode() {
            return fTokenNode;
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return super.hashCode();
        }

        /**
         * Returns the value corresponding to the current token.
         * 
         * @return the value corresponding to the current token
         */
        public boolean hasTokenValue() {
            return fTokenNode != null && fTokenNode.hasTokenValue();
        }

        /**
         * This method seeks the next node in the token tree and returns
         * <code>true</code> if such a node was found. Otherwise this handler is
         * considered as invalid.
         * 
         * @param ch the next character in the text
         * @return <code>true</code> if this handler was successfully updated
         */
        public boolean shift(char ch) {
            if (fTokenNode == null) {
                return false;
            }
            fTokenNode = fTokenNode.getChild(ch, false);
            return fTokenNode != null;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "TokenTreePointer[" + fTokenNode + "]";
        }
    }

    /**
     * The final character in the stream
     */
    public final static char END_CHAR = '\0';

    /**
     * The end character of phrases. Each end invalidates all found tokens.
     */
    public final static char END_OF_PHRASE = '.';

    /**
     * The current character
     */
    private char fCh = END_CHAR;

    /**
     * The listener used to report about found tokens.
     */
    private CompositeTokenListener<S> fListener = new CompositeTokenListener<S>();

    /**
     * The next character in the stream.
     */
    private char fNextCh;

    /**
     * Sets of handlers corresponding to new tokens.
     */
    private Set<TokenDetector.TokenTreePointer<S>> fPointers = new LinkedHashSet<TokenDetector.TokenTreePointer<S>>();

    /**
     * The position of the current character in the stream.
     */
    private int fPos = -1;

    /**
     * The number of already found space symbols. Used to normalize spaces.
     */
    private int fSpaceCounter = 0;

    /**
     * The token tree containing the dictionary of tokens.
     */
    private TokenTree<S> fTree;

    /**
     * The default constructor initialising the dictionary.
     * 
     * @param tree the token tree
     */
    public TokenDetector(TokenTree<S> tree) {
        fTree = tree;
    }

    /**
     * The default constructor initialising the dictionary and listener.
     * 
     * @param tree the token tree
     * @param listener the listener used to notify about found tokens
     */
    public TokenDetector(TokenTree<S> tree, ITokenListener<S> listener) {
        fTree = tree;
        addListener(listener);
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#addListener(org.mandriva.doc4.entities.ITokenListener)
     */
    public void addListener(ITokenListener<S> listener) {
        fListener.addListener(listener);
    }

    /**
     * Returns a filtered character.
     * 
     * @param ch the character to filter
     * @return a filtered character
     */
    protected char filterCharacter(char ch) {
        //ch = Character.toLowerCase(ch);
        return ch;
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#finish()
     */
    public void finish() {
        update(END_CHAR);
    }

    /**
     * @return a space character
     */
    protected char getSpaceChar() {
        return ' ';
    }

    /**
     * Returns <code>true</code> if the given character corresponds to the end
     * of a phrase.
     * 
     * @param ch the character to check
     * @return <code>true</code> if the given character corresponds to the end
     *         of a phrase
     */
    protected boolean isEndOfPhrase(char ch) {
        return ch == END_OF_PHRASE || ch == END_CHAR;
    }

    /**
     * Returns <code>true</code> if the given character corresponds to a space
     * symbol.
     * 
     * @param ch the character to check
     * @return <code>true</code> if the given character corresponds to a space
     *         symbol
     */
    protected boolean isSpace(char ch) {
        return Character.isSpaceChar(ch)
            || ch == '\n'
            || ch == '\r'
            || ch == '\t'
            || ch == '-'
            || ch == '_'
            || ch == '\''
            || ch == '\"'
            || ch == '~';
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#removeListener(org.mandriva.doc4.entities.ITokenListener)
     */
    public synchronized void removeListener(ITokenListener<S> listener) {
        fListener.removeListener(listener);
    }

    /**
     * @see org.mandriva.doc4.entities.ITokenDetector#update(char)
     */
    public void update(char ch) {
        fCh = fNextCh;
        fNextCh = filterCharacter(ch);

        char currentChar = fCh;
        if (isEndOfPhrase(currentChar)) {
            fPointers.clear();
            fSpaceCounter = 0;
        } else {
            boolean isSpace = isSpace(currentChar);
            if (isSpace) {
                currentChar = getSpaceChar();
                fSpaceCounter++;
            } else {
                if (fSpaceCounter > 0 || fPos == 0) {
                    TokenDetector.TokenTreePointer<S> handler = new TokenDetector.TokenTreePointer<S>(
                        fTree.fRoot,
                        fPos);
                    fPointers.add(handler);
                }
                fSpaceCounter = 0;
            }
            if (fSpaceCounter <= 1) {
                Set<TokenDetector.TokenTreePointer<S>> toRemove = null;
                for (TokenDetector.TokenTreePointer<S> handler : fPointers) {
                    if (handler.shift(currentChar)) {
                        // We can report about a new token only at the end of
                        // the word or a phrase
                        if (isEndOfPhrase(fNextCh) || isSpace(fNextCh)) {
                            if (handler.hasTokenValue()) {
                                TokenNode<S> node = handler.getTokenNode();
                                Range range = new Range(
                                    handler.getStartPos(),
                                    fPos);
                                fListener.onToken(range, node);
                            }
                        }
                    } else {
                        if (toRemove == null) {
                            toRemove = new HashSet<TokenDetector.TokenTreePointer<S>>();
                        }
                        toRemove.add(handler);
                    }
                }
                if (toRemove != null) {
                    for (TokenDetector.TokenTreePointer<S> handler : toRemove) {
                        fPointers.remove(handler);
                    }
                }
            }
        }
        fPos++;
    }
}