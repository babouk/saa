package saa;

import java.util.ArrayList;
import java.util.List;

/**
 * This structure is used to keep in memory one letter and all subnodes starting
 * with this letter. All tokens of a dictionary are represented as a tree where
 * each letter of each token corresponds to a node. Each node corresponds to a
 * specific sequence of letters.
 * <p>
 * Example:
 * </p>
 * 
 * <pre>
 * Tokens:
 * "abc"    => obj1
 * "abd"    => obj2
 * "abmo"   => obj3
 * "abx"    => obj4
 * 
 * The representation of these tokens in form of a tree: 
 *                  [a:null]
 *                     |
 *                  [b:null]
 *                     |
 *       +--------+----+---+--------+
 *       |        |        |        |
 *    [c:obj1] [d:obj2] [m:null] [x:obj4]
 *                         |
 *                      [m:obj3]
 * </pre>
 * 
 * @author kotelnikov
 * @param <S> the type of objects associated with each token.
 */
public class TokenNode<S> implements Comparable<TokenNode<S>> {

    /**
     * @param <X>
     * @param a the first value to compare
     * @param b the second value to compare
     * @return the integer value defininig the result of comparision betwee n
     *         the given values.
     */
    private static <X> int compare(TokenNode<X> a, TokenNode<X> b) {
        if (a == null || b == null) {
            return a == null && b == null ? 0 : a != null ? 1 : -1;
        }
        return a.fCh - b.fCh;
    }

    /**
     * An utility method used to compare two objects. This method returns
     * <code>true</code> if both objects are <code>null</code> of if they are
     * equal.
     * 
     * @param first the first object to compare
     * @param second the second object to compare
     * @return <code>true</code> if both objects are <code>null</code> of they
     *         are equal
     */
    private static boolean eq(Object first, Object second) {
        return first == null || second == null ? first == second : first
            .equals(second);
    }

    /**
     * The character associated with this node
     */
    private char fCh;

    /**
     * List of children. It could be <code>null</code>.
     */
    private List<TokenNode<S>> fChildren;

    /**
     * The parent node. It is used to re-build the character sequence
     * corresponding to this node.
     */
    private TokenNode<S> fParent;

    /**
     * Object associated with the token. If this value is <code>null</code> then
     * the node does not correspond to a named entity.
     */
    private S fValue;

    /**
     * The default constructor.
     * 
     * @param parent the parent node
     * @param ch the character corresponding to this node;
     */
    public TokenNode(TokenNode<S> parent, char ch) {
        fParent = parent;
        fCh = ch;
    }

    /**
     * Adds a new child in the specified position
     * 
     * @param pos the position of the child to add
     * @param child a new child to add
     */
    protected void addChild(int pos, TokenNode<S> child) {
        if (child == null) {
            return;
        }
        if (fChildren == null) {
            fChildren = new ArrayList<TokenNode<S>>();
        }
        fChildren.add(pos, child);
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(TokenNode<S> o) {
        TokenNode<S> first = fParent != null ? fParent : null;
        TokenNode<S> second = o.fParent != null ? o.fParent : null;
        int result = compare(first, second);
        if (result != 0) {
            return result;
        }
        result = compare(this, o);
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TokenNode<?>)) {
            return false;
        }
        TokenNode<?> o = (TokenNode<?>) obj;
        return fCh == o.fCh && eq(fValue, o.fValue) && eq(fParent, o.fParent);
    }

    /**
     * Seeks the position of a child node with the specified character. If there
     * is no child corresponding to the given character then this method returns
     * a negative value defining where such a node should be inserted. The
     * insert position in this case could be defined like this:
     * <code>pos = -(pos + 1)</code>
     * 
     * @param ch the character to seek
     * @return the position of a child node with the
     */
    private int findChildPos(char ch) {
        int low = 0;
        int size = fChildren != null ? fChildren.size() : 0;
        int high = size - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            TokenNode<S> midVal = fChildren.get(mid);
            int cmp = midVal.fCh - ch;
            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                return mid; // key found
            }
        }
        return -(low + 1); // key not found
    }

    /**
     * Returns the character associated with this node
     * 
     * @return the character associated with this node
     */
    public char getCh() {
        return fCh;
    }

    /**
     * This method seeks and returns a child node corresponding to the specified
     * character. If there is no sub-nodes with the given character and the
     * <code>create</code> parameter is <code>true</code> then a new node will
     * be created and returned.
     * 
     * @param ch the character corresponding to the node
     * @param create if this flag is <code>true</code> and there is no child
     *        corresponding to the given character then a new child is created
     *        and inserted
     * @return a child node corresponding to the specified character
     */
    public TokenNode<S> getChild(char ch, boolean create) {
        int pos = findChildPos(ch);
        TokenNode<S> child = null;
        if (pos < 0) {
            if (create) {
                child = new TokenNode<S>(this, ch);
                pos = -(pos + 1);
                addChild(pos, child);
            }
        } else {
            child = getChild(pos);
        }
        return child;
    }

    /**
     * Returns a child node from the specified position
     * 
     * @param pos the position of the child node
     * @return a child node from the specified position
     */
    public TokenNode<S> getChild(int pos) {
        int size = fChildren != null ? fChildren.size() : 0;
        return pos >= 0 && pos < size ? fChildren.get(pos) : null;
    }

    /**
     * Returns a list of all children of this node
     * 
     * @return a list containing all child nodes
     */
    public List<TokenNode<S>> getChildren() {
        return fChildren;
    }

    /**
     * Returns the parent node
     * 
     * @return the parent node
     */
    public TokenNode<S> getParent() {
        return fParent;
    }

    /**
     * Returns the token corresponding to this node. To build such a token this
     * method uses parent nodes.
     * 
     * @return the token corresponding to this node
     */
    public String getToken() {
        StringBuilder buf = new StringBuilder();
        TokenNode<S> node = this;
        // If the parent of the node is null then it is the root.
        while (node != null && node.fParent != null) {
            char ch = node.fCh;
            buf.insert(0, ch);
            node = node.getParent();
        }
        return buf.toString();

    }

    /**
     * Returns the value associated with this node
     * 
     * @return the value associated with this node
     */
    public S getValue() {
        return fValue;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int a = fCh;
        int b = fValue != null ? fValue.hashCode() : 0;
        return a ^ b;
    }

    /**
     * Returns <code>true</code> if this node has an associated user-defined
     * value.
     * 
     * @return <code>true</code> if this node has an associated user-defined
     *         value
     */
    public boolean hasTokenValue() {
        return fValue != null;
    }

    /**
     * Sets a new character value associated with this node.
     * 
     * @param ch the character to set
     */
    public void setCh(char ch) {
        fCh = ch;
    }

    /**
     * Sets a new value associated with this node.
     * 
     * @param value the value to set
     */
    public void setValue(S value) {
        fValue = value;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getToken();
    }
}