package saa;

import saa.ITokenListener.Range;

/**
 * Objects of this type are used to detect tokens (named entities) in texts.
 * Token detectors consume characters provided by clients and notify registered
 * listeners when a new token is found. Example of usage:
 * 
 * <pre>
 * TokenTree<MyClass> tree = new TokenTree<MyClass>();
 * tree.add("this", new MyClass(123));
 * tree.add("text", new MyClass(345));
 * ITokenDetector<MyClass> detector = 
 *      new TokenDetector<MyClass>(tree, new ITokenListener<MyClass>() {
 *          public void onToken(Range range, String token, MyClass value) {
 *              System.out.println(range + "\t" + token + "\t" + value);
 *          }
 *      });
 *  
 * String text = "this is my text";
 * for (int i=0; i<text.length(); i++) {
 *     char ch = text.charAt(i);
 *     // Push the character to the token detector.
 *     detector.update(ch);
 * }
 * // Finalize the process of token detection.
 * detector.finish();
 *   
 * </pre>
 * 
 * @author kotelnikov
 * @param <S> the type of objects associated with tokens
 */
public interface ITokenDetector<S> {

    /**
     * Adds a new listener to the internal list
     * 
     * @param listener the listener to add
     */
    void addListener(ITokenListener<S> listener);

    /**
     * Finializes the updating process.
     */
    void finish();

    /**
     * Removes the given listener from the internal list
     * 
     * @param listener the listener to remove
     */
    void removeListener(ITokenListener<S> listener);

    /**
     * Updates the internal state of this class. When the specified character
     * corresponds to the end of a token then this token is reported to the
     * registered listener using the
     * {@link ITokenListener#onToken(Range, TokenNode)} method.
     * 
     * @param ch the next character in the stream
     */
    void update(char ch);

}