package saa;

/**
 * Instances of this type are used to notify about entities found in text
 * streams.
 * 
 * @author kotelnikov
 * @param <S> the type of additional objects associated with each found token
 */
public interface ITokenListener<S> {

    /**
     * This structure is used to define the position of found tokens in the
     * initial text stream. The first character in the text stream corresponds
     * to the position 0.
     * 
     * @author kotelnikov
     */
    public static class Range {
        /**
         * The end position of the range
         */
        private int fEndPos;

        /**
         * Start position of the range
         */
        private int fStartPos;

        /**
         * This constructor initializes the internal fields - the start and end
         * position of the range.
         * 
         * @param startPos the start position of the range
         * @param endPos the end position of the range
         */
        public Range(int startPos, int endPos) {
            fStartPos = startPos;
            fEndPos = endPos;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof ITokenListener.Range)) {
                return false;
            }
            ITokenListener.Range o = (ITokenListener.Range) obj;
            return fStartPos == o.fStartPos && fEndPos == o.fEndPos;
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return fStartPos ^ fEndPos;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "[" + fStartPos + ":" + fEndPos + "]";
        }

    }

    /**
     * This method is called to notify that a new token was found in the text
     * stream. Note that the real characters in the stream defined by the given
     * range could be different from the "canonical" token (the value returned
     * by the {@link TokenNode#getToken()} method) - the real text stream can
     * have spaces and non-counted symbols between words while the canonical
     * token does not have these symbols.
     * 
     * @param range the range defining the position of the token in the stream;
     * @param node the found token corresponding to the specified range
     */
    void onToken(ITokenListener.Range range, TokenNode<S> node);
}