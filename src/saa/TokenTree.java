/**
 * 
 */
package saa;

/**
 * Instances of this type keep in memory the "dictionary" of named entities.
 * TokenTrees are used by {@link TokenDetector} to find named entities in texts.
 * Each token (term) of the dictionary is associated with an object containing
 * the "description" of this term.
 * 
 * @author kotelnikov
 * @param <S> the type of objects associated with tokens
 */
public class TokenTree<S> {

    /**
     * The root node of the tree.
     */
    protected TokenNode<S> fRoot = new TokenNode<S>(null, '\0');

    /**
     * 
     */
    public TokenTree() {
    }

    /**
     * This method adds a new token in the tree and associated it with the
     * specified value.
     * 
     * @param token the token to add
     * @param value the value associated with this token
     */
    public void add(String token, S value) {
        TokenNode<S> node = getNode(token, true);
        S oldValue = node.getValue();
        S newValue = updateValue(oldValue, value);
        node.setValue(newValue);
    }

    /**
     * Returns the value corresponding to the given token or <code>null</code>
     * if such a token does not exist in the tree.
     * 
     * @param token the token to seek
     * @return a value associated with the specified token
     */
    public S get(String token) {
        TokenNode<S> node = getNode(token, false);
        return node != null ? node.getValue() : null;
    }

    /**
     * Searches and returns a node corresponding to the given token.
     * 
     * @param token the token used to search nodes
     * @param create if this flag is <code>true</code> then a new node is
     *        created for the given token
     * @return the node corresponding to the given token
     */
    public TokenNode<S> getNode(String token, boolean create) {
        TokenNode<S> node = fRoot;
        for (int i = 0; node != null && i < token.length(); i++) {
            char ch = token.charAt(i);
            TokenNode<S> child = node.getChild(ch, create);
            node = child;
        }
        return node;
    }

    /**
     * Returns the root node of the tree.
     * 
     * @return the root node of the tree.
     */
    public TokenNode<S> getRootNode() {
        return fRoot;
    }

    /**
     * This method could be overloaded in subclasses to "merge" already existing
     * value in the tree with the new one.
     * 
     * @param oldValue the old value already defined in the tree
     * @param newValue the new value to set
     * @return the merged value used to set in the tree
     */
    protected S updateValue(S oldValue, S newValue) {
        return newValue;
    }

}
