package saa.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Test for org.mandriva.doc4.entities");
        //$JUnit-BEGIN$
        suite.addTestSuite(EntitiesIOTest.class);
        suite.addTestSuite(BigTreeTest.class);
        suite.addTestSuite(AnnotatorTest.class);
        //$JUnit-END$
        return suite;
    }

}
