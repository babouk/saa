/**
 * 
 */
package saa.tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import saa.TokenTree;
import saa.TokensIO;

/**
 * @author kotelnikov
 */
public class EntitiesIOTest extends AbstractEntitiesTest {

    /**
     * @param name
     */
    public EntitiesIOTest(String name) {
        super(name);
    }

    public void test() throws IOException {
        Map<String, String> tokens = new LinkedHashMap<String, String>();
        TokenTree<String> tree = new TokenTree<String>();
        int count = 1000;
        for (int i = 0; i < count; i++) {
            String token = newRandomString();
            String value = token;
            tokens.put(token, value);
            tree.add(token, value);
        }
        TokensIO<String> io = new TokensIO<String>() {
            @Override
            protected String readValue(DataInput input) throws IOException {
                String str = input.readUTF();
                if (str.equals("\0")) {
                    return null;
                }
                return str;
            }

            @Override
            protected void writeValue(String value, DataOutput writer)
                throws IOException {
                if (value == null) {
                    value = "\0";
                }
                writer.writeUTF(value);
            }
        };

        OutputStream out = new ByteArrayOutputStream();
        try {
            DataOutputStream writer = new DataOutputStream(out);
            io.write(tree, writer);
        } finally {
            out.close();
        }

        TokenTree<String> testTree = new TokenTree<String>();

        InputStream input = new ByteArrayInputStream(
            ((ByteArrayOutputStream) out).toByteArray());
        try {
            DataInputStream reader = new DataInputStream(input);
            io.read(testTree, reader);
        } finally {
            input.close();
        }

        for (Map.Entry<String, String> entry : tokens.entrySet()) {
            String token = entry.getKey();
            String value = entry.getValue();
            String test = testTree.get(token);
            assertEquals(value, test);
        }

    }
}
