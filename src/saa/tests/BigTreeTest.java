package saa.tests;

import java.util.Random;

import saa.TokenTree;

import junit.framework.TestCase;

public class BigTreeTest extends TestCase {

    private static char[] CHARS = "abcdef ghijkl mnopqr stuvxy z01234 56789"
        .toCharArray();

    private Random fRandom = new Random(System.currentTimeMillis());

    public BigTreeTest(String name) {
        super(name);
    }

    private String newRandomString(int minLen, int maxLen) {
        int len = fRandom.nextInt(maxLen - minLen) + minLen;
        byte[] buf = new byte[len];
        fRandom.nextBytes(buf);
        for (int i = 0; i < len; i++) {
            buf[i] = (byte) CHARS[(0xFF & buf[i]) % CHARS.length];
        }
        return new String(buf);
    }

    public void testBigNumber() throws Exception {
        int count = 50000;
        TestReport report = new TestReport();
        report.printLine();
        report.println("Inserting tokens...");
        report.start();
        TokenTree<Object> tree = new TokenTree<Object>();
        for (int i = 0; i < count; i++) {
            String token = newRandomString(3, 50);
            Object value = new Object();
            tree.add(token, value);
            if (i % 10000 == 0) {
                report.println("Inserted " + i + " items...");
            }
        }
        report.report(count);
    }

}
