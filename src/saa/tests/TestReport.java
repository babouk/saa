package saa.tests;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * @author MikhailKotelnikov
 */
public class TestReport {

    /**
     * Start time. This field is used by {@link #start()},{@link #stop()}and
     * {@link #report(int)}methods to generate performance reports.
     */
    private long fStartTime;

    protected PrintWriter fWriter;

    /**
     * 
     */
    public TestReport() {
        this(System.out);
    }

    /**
     * @param out
     */
    public TestReport(OutputStream out) {
        super();
        try {
            fWriter = new PrintWriter(
                new OutputStreamWriter(out, "UTF-8"),
                true);
        } catch (UnsupportedEncodingException e) {
            // This code is unreachable - "UTF-8" always exists
        }
        start();
    }

    /**
     * @param writer
     */
    public TestReport(PrintWriter writer) {
        fWriter = writer;
        start();
    }

    /**
     * Method printLine.
     */
    public void printLine() {
        println("");
        println("-------------------------------------------------------");
    }

    /**
     * Method println.
     * 
     * @param msg
     */
    public void println(String msg) {
        fWriter.println(msg);
        fWriter.flush();
    }

    /**
     * Method report.
     * 
     * @param size
     */
    public void report(int size) {
        report(stop(), size);
    }

    /**
     * Method report.
     * 
     * @param waitTime
     * @param size
     */
    protected void report(long waitTime, int size) {
        String hitPerMsec = (waitTime > 0)
            ? "" + (float) (size / waitTime)
            : "?";
        String hitPerSec = (waitTime > 0) ? ""
            + (float) (size * 1000 / waitTime) : "?";
        println("  Count: " + size);
        println("   Time: "
            + waitTime
            + " msec. ("
            + ((float) waitTime / 1000)
            + " sec.)");
        println("  Speed: "
            + hitPerMsec
            + " hit/msec ("
            + hitPerSec
            + " hit/sec)");
    }

    /**
     * Method start.
     */
    public void start() {
        fStartTime = System.currentTimeMillis();
    }

    /**
     * Method stop.
     * 
     * @return long - count of milliseconds
     */
    public long stop() {
        return System.currentTimeMillis() - fStartTime;
    }

}
