/**
 * 
 */
package saa.tests;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import junit.framework.TestCase;

import saa.ITokenDetector;
import saa.ITokenListener;
import saa.ITokenListener.Range;
import saa.TokenDetector;
import saa.TokenTree;
import saa.TokenNode;

/**
 * @author kotelnikov
 */
public abstract class AbstractEntitiesTest extends TestCase {

    protected static char[] CHARS = "abcdef ghijkl mnopqr stuvxy z01234 56789"
        .toCharArray();

    protected Random fRandom = new Random(System.currentTimeMillis());

    protected AbstractEntitiesTest(String name) {
        super(name);
    }

    protected void addRange(
        Map<Range, String> ranges,
        int start,
        int end,
        String token) {
        ranges.put(new Range(start, end), token);
    }

    protected void fillTokens(
        Map<String, Object> map,
        TokenTree<Object> tree,
        String... tokens) {
        for (String token : tokens) {
            Object value = new Object();
            tree.add(token, value);
            map.put(token, value);
        }
    }

    protected Map<Range, String> getRanges(String text, TokenTree<Object> tree) {
        final Map<Range, String> rangeTest = new LinkedHashMap<Range, String>();
        ITokenDetector<Object> detector = new TokenDetector<Object>(
            tree,
            new ITokenListener<Object>() {
                public void onToken(Range range, TokenNode<Object> node) {
                    rangeTest.put(range, node.getToken());
                }
            });
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            detector.update(ch);
        }
        detector.finish();
        return rangeTest;
    }

    protected String newRandomString() {
        int maxLen = 100;
        int minLen = 3;
        int len = fRandom.nextInt(maxLen - minLen) + minLen;
        byte[] buf = new byte[len];
        fRandom.nextBytes(buf);
        for (int i = 0; i < len; i++) {
            buf[i] = (byte) CHARS[(0xFF & buf[i]) % CHARS.length];
        }
        return new String(buf);
    }

}
