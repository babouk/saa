package saa.tests;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import saa.ITokenDetector;
import saa.ITokenListener.Range;
import saa.TokenDetector;
import saa.TokenScorrer;
import saa.TokenScorrer.Score;
import saa.TokenTree;

/**
 * @author kotelnikov
 */
public class AnnotatorTest extends AbstractEntitiesTest {

    public AnnotatorTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    public void test() throws Exception {
        Map<String, Object> tokens = new LinkedHashMap<String, Object>();
        TokenTree<Object> tree = new TokenTree<Object>();
        fillTokens(tokens, tree, "first", "second", "third", "fourth", "fifth");
        for (Map.Entry<String, Object> entry : tokens.entrySet()) {
            String token = entry.getKey();
            Object value = entry.getValue();
            Object test = tree.get(token);
            assertEquals(value, test);
        }
    }

    public void testBigNumber() throws Exception {
        int count = 10000;
        TestReport report = new TestReport();
        report.printLine();
        report.println("Inserting tokens...");
        report.start();
        Map<String, Object> tokens = new HashMap<String, Object>();
        TokenTree<Object> tree = new TokenTree<Object>();
        for (int i = 0; i < count; i++) {
            String token = newRandomString();
            Object value = new Object();
            tokens.put(token, value);
            tree.add(token, value);
            if (i % 10000 == 0) {
                report.println("Inserted " + i + " items...");
            }
        }
        report.report(count);

        report.println("Search for tokens...");
        report.start();
        for (Map.Entry<String, Object> entry : tokens.entrySet()) {
            String token = entry.getKey();
            Object value = entry.getValue();
            Object test = tree.get(token);
            assertEquals(value, test);
        }
        report.report(count);
        report.printLine();
    }

    public void testLongText() {
        System.out
            .println("==================================================");

        String text = "Feature Overview\n"
            + "\n"
            + "* Music Collection:\n"
            + "You Semantic Web a huge music library and want to locate tracks quickly? Let amaroK's\n"
            + "powerful Collection take care of that! It's a database powered music     store,\n"
            + "which keeps track of your complete music \n"
            + "  library, allowing you to find any\n"
            + "title in a matter of seconds.\n"
            + "\n"
            + "* Intuitive User Interface:\n"
            + "You will be amazed to see how easy amaroK is to use! Simply drag-and-drop files\n"
            + "into the playlist. No hassle with complicated  buttons or tangled menus.\n"
            + "Listening to music has never been easier!\n"
            + "\n"
            + "* Streaming Radio:\n"
            + "Web streams take radio to the next level: Listen to thousands of great radio\n"
            + "stations on the internet, for free! amaroK provides excellent streaming\n"
            + "support, with advanced features, such as displaying titles of the currently\n"
            + "playing songs.\n"
            + "\n"
            + "* Context Browser:\n"
            + "This tool provides useful information on the music you are currently listening\n"
            + "to, and can make listening suggestions, based on your personal music taste. An\n"
            + "innovate and unique feature.\n"
            + "\n"
            + "* Visualizations:\n"
            + "amaroK is compatible with XMMS visualization plugins. Allows you to use the\n"
            + "great number of stunning visualizations available on the net. 3d visualizations\n"
            + "with OpenGL are a great way to enhance your music experience.";

        // text =
        // "Player is a network server for robot control. Running on your robot, Player\r\n"
        // +
        // "provides a clean and simple interface to the robot's sensors and actuators\r\n"
        // +
        // "over the IP network. Your client program talks to Player over a TCP socket,\r\n"
        // +
        // "reading data from sensors, writing commmands to actuators, and configuring\r\n"
        // + "devices on the fly. Player supports a variety of robot hardware.";

        System.out.println(text);
        Map<String, Object> tokens = new LinkedHashMap<String, Object>();
        TokenTree<Object> tree = new TokenTree<Object>();
        fillTokens(
            tokens,
            tree,
            "player",
            "database",
            "library",
            "streaming",
            "Semantic Web",
            "radio",
            "music",
            "music library",
            "playlist",
            "music store",
            "OpenGL");

        ITokenDetector<Object> detector = new TokenDetector<Object>(tree);
        TokenScorrer<Object> scorrer = new TokenScorrer<Object>(detector);
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            scorrer.update(ch);
        }
        scorrer.finish();

        List<Score<Object>> scores = scorrer.getScores();

        System.out.println();
        System.out.println("Found tokens:");
        for (Score<Object> score : scores) {
            System.out.println(score.getToken() + " - " + score.getRanges());
        }
    }

    public void testTextScan() {
        Map<Range, String> r = new LinkedHashMap<Range, String>();
        addRange(r, 4, 8, "first");
        addRange(r, 14, 19, "second");
        addRange(r, 14, 25, "second first");
        addRange(r, 21, 25, "first");
        addRange(r, 34, 39, "fourth");
        addRange(r, 43, 47, "first");
        testTextScan("abc first cde second first lqsdjg fourth q first", r);

        r.clear();
        addRange(r, 4, 8, "first");
        addRange(r, 14, 19, "second");
        addRange(r, 22, 26, "first");
        addRange(r, 35, 40, "fourth");
        addRange(r, 44, 48, "first");
        testTextScan("abc first cde second. first lqsdjg fourth q first", r);

        r.clear();
        addRange(r, 4, 8, "first");
        addRange(r, 14, 19, "second");
        addRange(r, 14, 28, "second first");
        addRange(r, 24, 28, "first");
        addRange(r, 37, 42, "fourth");
        addRange(r, 46, 50, "first");
        testTextScan("abc first cde second    first lqsdjg fourth q first", r);

        // Check that empty spaces and new line characters are skipped properly
        r.clear();
        addRange(r, 5, 9, "music");
        addRange(r, 5, 20, "music library");
        addRange(r, 14, 20, "library");
        testTextScan("abc  music    library cde", r);
        testTextScan("abc  music \n\n library cde", r);
    }

    public void testTextScan(String text, Map<Range, String> control) {
        TokenTree<Object> tree = new TokenTree<Object>();
        for (String token : control.values()) {
            Object value = new Object();
            tree.add(token, value);
        }

        Map<Range, String> testRanges = getRanges(text, tree);
        assertEquals(control.toString(), testRanges.toString());
    }
}
