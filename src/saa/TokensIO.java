/**
 * 
 */
package saa;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;


/**
 * @author kotelnikov
 */
public abstract class TokensIO<S> {

    /**
     * 
     */
    public TokensIO() {
    }

    public void read(TokenTree<S> tree, DataInput input) throws IOException {
        TokenNode<S> root = tree.getRootNode();
        readChildren(root, input);
    }

    private void readChildren(TokenNode<S> parent, DataInput input)
        throws IOException {
        int childCount = input.readInt();
        for (int i = 0; i < childCount; i++) {
            char ch = input.readChar();
            TokenNode<S> child = parent.getChild(ch, true);
            S value = readValue(input);
            if (value != null) {
                child.setValue(value);
            }
            readChildren(child, input);
        }
    }

    protected abstract S readValue(DataInput input) throws IOException;

    public void write(TokenTree<S> tree, DataOutput writer) throws IOException {
        TokenNode<S> root = tree.getRootNode();
        writeChildren(root, writer);
    }

    private void writeChildren(TokenNode<S> root, DataOutput writer)
        throws IOException {
        List<TokenNode<S>> children = root.getChildren();
        int count = children != null ? children.size() : 0;
        writer.writeInt(count);
        for (int i = 0; i < count; i++) {
            TokenNode<S> child = children.get(i);
            char ch = child.getCh();
            writer.writeChar(ch);
            S value = child.getValue();
            writeValue(value, writer);
            writeChildren(child, writer);
        }
    }

    protected abstract void writeValue(S value, DataOutput writer)
        throws IOException;

}
