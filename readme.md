# Sáa Named-Entity Recognizer

Sáa is a lightweight [named-entity recognition](https://en.wikipedia.org/wiki/Named-entity_recognition) (NER) utility written in Java by [mkotelnikov](https://github.com/mkotelnikov). 

[_Sáa_](https://fr.wiktionary.org/wiki/s%C3%A1a#Yahadian) means "water" in [Yahadian](https://en.m.wikipedia.org/wiki/Yahadian_language), whose language ISO code is NER. As a named-entity, [Saa](https://en.m.wikipedia.org/wiki/Saa) has several meanings, including a reference to the [Saa language](https://en.m.wikipedia.org/wiki/Saa_language).

## Usage

The command line below outputs a representation of the named-entity occurrences, with their position:

```
java -cp saar.jar saa.Saa <input-text> <comma-separeted-named-entities>
```

```
java -cp saar.jar saa.Saa "when the music is over jim morrison jim hello bonjour alpha beta" "music, jim"
```

```
jim - [[23:25], [36:38]]
music - [[9:13]]
```